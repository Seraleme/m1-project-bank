﻿using System;
using System.Collections.Generic;
using M1_Bank_Project.dataAccess;
using M1_Bank_Project.model.deserialize;
using M1_Bank_Project.model.user;

namespace M1_Bank_Project.services
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class BankService
    {
        public IClientDataAccess _clientDataAccess;
        public DeserializeCurrencies currencies;
        private Admin admin;

        public BankService(IClientDataAccess clientDataAccess, DeserializeCurrencies currencies)
        {
            this.currencies = currencies;
            this._clientDataAccess = clientDataAccess;
            admin = new Admin();
        }

        public Client createClient(string firstname, string lastname, string defaultCurrency)
        {
            return _clientDataAccess.CreateClient(firstname, lastname, defaultCurrency);
        }

        public void deleteClient(Guid guid)
        {
            _clientDataAccess.DeleteClient(guid);
        }

        public List<Client> getAllClient()
        {
            return _clientDataAccess.GetAll();
        }

        public void toggleBlock(Client client)
        {
            client.block = !client.block;
            if (client.block == false)
                client.tries = 0;
            _clientDataAccess.UpdateClient(client);
        }

        public void addMoney(Client client, decimal amount, string keyCurrency)
        {
            if (amount == 0)
                return;

            if (client.currencies.ContainsKey(keyCurrency))
            {
                client.currencies[keyCurrency] += amount;
                if (client.currencies[keyCurrency] == 0)
                    client.currencies.Remove(keyCurrency);
            }
            else
                client.currencies.Add(keyCurrency, amount);

            _clientDataAccess.UpdateClient(client);
        }

        public IUser connectUser(string guidString, string pin)
        {
            if (admin.firstname.Equals(guidString))
            {
                if (admin.pin.Equals(pin))
                    return admin;
                throw new Exception("Incorrect admin password.");
            }

            Guid guid;
            try
            {
                guid = Guid.Parse(guidString);
            }
            catch (Exception)
            {
                throw new Exception("Invalid guid format.");
            }

            Client client;
            try
            {
                client = _clientDataAccess.GetClient(guid);
                if (client == null)
                    return null;

                if (client.pin.Equals(pin) && !client.block)
                {
                    if (client.tries != 0)
                    {
                        client.tries = 0;
                        _clientDataAccess.UpdateClient(client);
                    }

                    return client;
                }
            }
            catch (Exception)
            {
                throw new Exception("An error occurred during data recovery.");
            }

            if (client.block)
                throw new Exception($"Your account is blocked.");

            client.tries++;
            if (client.tries == 3)
                client.block = true;
            _clientDataAccess.UpdateClient(client);

            if (client.tries == 3)
                throw new Exception("Your account has been blocked.");

            throw new Exception($"Incorrect password, {client.tries} tries.");
        }
    }
}