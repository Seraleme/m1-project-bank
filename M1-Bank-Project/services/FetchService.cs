﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using M1_Bank_Project.model.deserialize;
using Newtonsoft.Json;

namespace M1_Bank_Project.services
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class FetchService
    {
        private static readonly HttpClient client = new HttpClient();

        public async Task<T> fetchData<T>(params Object[] args) where T : IDeserialize, new()
        {
            string json = await client.GetStringAsync(new T().getURL(args));
            return JsonConvert.DeserializeObject<T>(json);
        }

        public async Task<Dictionary<string, T>> fetchDataDictionary<T>(params Object[] args)
            where T : IDeserialize, new()
        {
            string json = await client.GetStringAsync(new T().getURL(args));
            return JsonConvert.DeserializeObject<Dictionary<string, T>>(json);
        }

        public async Task<T> fetchDataUrl<T>(string url)
        {
            string json = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public BitmapImage fetchChartImage(string json)
        {
            return fetchImage($"https://quickchart.io/chart?c={json}");
        }

        public BitmapImage fetchImage(string url)
        {
            var fullFilePath = @url;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
            bitmap.EndInit();
            return bitmap;
        }

        public BitmapImage fetchImageRelative(string url)
        {
            var fullFilePath = @url;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.RelativeOrAbsolute);
            bitmap.EndInit();
            return bitmap;
        }
    }
}