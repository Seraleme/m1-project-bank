﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using M1_Bank_Project.model.user;
using Newtonsoft.Json;

namespace M1_Bank_Project.dataAccess
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class ClientJsonAccess : IClientDataAccess
    {
        private const string PATH = @"assets/clients.json";

        public ClientJsonAccess()
        {
            if (!File.Exists(PATH))
                File.CreateText(PATH);
        }

        private List<Client> readJson()
        {
            try
            {
                var listClient = JsonConvert.DeserializeObject<List<Client>>(File.ReadAllText(PATH));
                if (listClient == null)
                    return new List<Client>();
                return listClient;
            }
            catch (Exception)
            {
                return new List<Client>();
            }
        }

        private void saveJson(List<Client> listClient)
        {
            File.WriteAllText(PATH, JsonConvert.SerializeObject(listClient, Formatting.Indented));
        }

        public List<Client> GetAll()
        {
            return readJson();
        }

        public Client CreateClient(string firstname, string lastname, string defaultCurrency)
        {
            Client client = new Client(firstname, lastname, defaultCurrency);
            client.currencies.Add(defaultCurrency, 0);

            List<Client> listClient = readJson();

            if (listClient == null)
                return null;

            listClient.Add(client);
            saveJson(listClient);
            return client;
        }

        public Client GetClient(Guid guid)
        {
            List<Client> listClient = readJson();

            if (listClient == null)
                return null;

            var findIndex = listClient.FindIndex(x => x.guid == guid);
            return findIndex != -1 ? listClient[findIndex] : null;
        }

        public void UpdateClient(Client client)
        {
            List<Client> listClient = readJson();

            if (listClient == null)
                return;

            foreach (var currentClient in listClient.Where(currentClient => currentClient.guid == client.guid))
            {
                currentClient.firstname = client.firstname;
                currentClient.lastname = client.lastname;
                currentClient.pin = client.pin;
                currentClient.currencies = client.currencies;
                currentClient.mainCurrencySymbol = client.mainCurrencySymbol;
                currentClient.block = client.block;
                currentClient.tries = client.tries;
                saveJson(listClient);
                break;
            }
        }

        public void DeleteClient(Guid guid)
        {
            List<Client> listClient = readJson();

            if (listClient == null)
                return;

            var index = listClient.FindIndex(x => x.guid == guid);

            if (index == -1)
                return;

            listClient.RemoveAt(index);
            saveJson(listClient);
        }
    }
}