﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using M1_Bank_Project.model.user;

namespace M1_Bank_Project.dataAccess
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class ClientDBAccess : IClientDataAccess
    {
        public static string database = "assets/bank.db";
        private SQLiteConnection connection;

        public ClientDBAccess()
        {
            if (!File.Exists(database))
                throw new Exception("Database not found");
            connection = new SQLiteConnection($"Data Source={database}");
            connection.Open();
            GetAll();
        }

        public List<Client> GetAll()
        {
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Clients";

            SQLiteDataReader reader = command.ExecuteReader();
            List<Client> list = new List<Client>();
            while (reader.Read())
            {
                Guid guid = Guid.Parse(reader["client_id"].ToString() ?? throw new InvalidOperationException());

                Client client = new Client(
                    guid,
                    reader["firstname"].ToString() ?? throw new InvalidOperationException(),
                    reader["lastname"].ToString() ?? throw new InvalidOperationException(),
                    reader["pin"].ToString() ?? throw new InvalidOperationException(),
                    reader["currency"].ToString() ?? throw new InvalidOperationException(),
                    Convert.ToBoolean(reader["block"]),
                    Convert.ToInt32(reader["tries"]));

                command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Clients_Currencies WHERE client_id=@param1";
                command.Parameters.AddWithValue("@param1", guid.ToString());
                SQLiteDataReader reader1 = command.ExecuteReader();
                while (reader1.Read())
                    client.currencies.Add(reader1["currency_id"].ToString(),
                        Convert.ToDecimal(reader1["amount"]));

                list.Add(client);
            }

            return list;
        }

        public Client CreateClient(string firstname, string lastname, string defaultCurrency)
        {
            Client client = new Client(firstname, lastname, defaultCurrency);
            SQLiteCommand command = connection.CreateCommand();

            // Create User
            command.CommandText =
                @"INSERT INTO Clients (client_id, firstname, lastname, currency, pin) VALUES (@param1, @param2, @param3, @param4, @param5)";
            command.Parameters.AddWithValue("@param1", client.guid.ToString());
            command.Parameters.AddWithValue("@param2", client.firstname);
            command.Parameters.AddWithValue("@param3", client.lastname);
            command.Parameters.AddWithValue("@param4", client.mainCurrencySymbol);
            command.Parameters.AddWithValue("@param5", client.pin);
            command.ExecuteNonQuery();

            // Create Default user currency
            command = connection.CreateCommand();
            command.CommandText =
                @"INSERT OR IGNORE INTO Clients_Currencies (client_id, currency_id) VALUES (@param1,@param2)";
            command.Parameters.AddWithValue("@param1", client.guid.ToString());
            command.Parameters.AddWithValue("@param2", client.mainCurrencySymbol);
            command.ExecuteNonQuery();

            return client;
        }

        public Client GetClient(Guid guid)
        {
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = @"SELECT * FROM Clients WHERE client_id=@param1";
            command.Parameters.AddWithValue("@param1", guid.ToString());

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Client client = new Client(
                    Guid.Parse(reader["client_id"].ToString() ?? throw new InvalidOperationException()),
                    reader["firstname"].ToString() ?? throw new InvalidOperationException(),
                    reader["lastname"].ToString() ?? throw new InvalidOperationException(),
                    reader["pin"].ToString() ?? throw new InvalidOperationException(),
                    reader["currency"].ToString() ?? throw new InvalidOperationException(),
                    Convert.ToBoolean(reader["block"]),
                    Convert.ToInt32(reader["tries"]));

                command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Clients_Currencies WHERE client_id=@param1";
                command.Parameters.AddWithValue("@param1", guid.ToString());
                SQLiteDataReader reader1 = command.ExecuteReader();
                while (reader1.Read())
                    client.currencies.Add(reader1["currency_id"].ToString() ?? string.Empty,
                        Convert.ToDecimal(reader1["amount"]));

                return client;
            }

            return null;
        }

        public void UpdateClient(Client client)
        {
            SQLiteCommand command = connection.CreateCommand();

            command.CommandText =
                @"UPDATE Clients SET firstname=@param2 , lastname=@param3, currency=@param4, pin=@param5, block=@param6, tries==@param7 WHERE client_id=@param1";
            command.Parameters.AddWithValue("@param1", client.guid.ToString());
            command.Parameters.AddWithValue("@param2", client.firstname);
            command.Parameters.AddWithValue("@param3", client.lastname);
            command.Parameters.AddWithValue("@param4", client.mainCurrencySymbol);
            command.Parameters.AddWithValue("@param5", client.pin);
            command.Parameters.AddWithValue("@param6", client.block);
            command.Parameters.AddWithValue("@param7", client.tries);
            command.ExecuteNonQuery();

            foreach (var keyValuePair in client.currencies)
            {
                command = connection.CreateCommand();
                command.CommandText =
                    @"INSERT OR IGNORE INTO Clients_Currencies(client_id, currency_id, amount) VALUES (@param1,@param2,@param3)";
                command.Parameters.AddWithValue("@param1", client.guid.ToString());
                command.Parameters.AddWithValue("@param2", keyValuePair.Key);
                command.Parameters.AddWithValue("@param3", keyValuePair.Value);
                command.ExecuteNonQuery();

                command = connection.CreateCommand();
                command.CommandText =
                    @"UPDATE Clients_Currencies SET amount=@param3 WHERE client_id=@param1 AND currency_id=@param2";
                command.Parameters.AddWithValue("@param1", client.guid.ToString());
                command.Parameters.AddWithValue("@param2", keyValuePair.Key);
                command.Parameters.AddWithValue("@param3", keyValuePair.Value);
                command.ExecuteNonQuery();
            }
        }

        public void DeleteClient(Guid guid)
        {
            SQLiteCommand command = connection.CreateCommand();

            command.CommandText = "DELETE FROM Clients_Currencies WHERE client_id=@param1";
            command.Parameters.AddWithValue("@param1", guid.ToString());
            command.ExecuteNonQuery();

            command = connection.CreateCommand();
            command.CommandText = "DELETE FROM Clients WHERE client_id=@param1";
            command.Parameters.AddWithValue("@param1", guid.ToString());
            command.ExecuteNonQuery();
        }

        public void CloseConnection()
        {
            connection.Close();
        }
    }
}