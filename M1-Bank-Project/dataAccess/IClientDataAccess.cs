﻿using System;
using System.Collections.Generic;
using M1_Bank_Project.model.user;

namespace M1_Bank_Project.dataAccess
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public interface IClientDataAccess
    {
        List<Client> GetAll();
        Client CreateClient(string firstname, string lastname, string defaultCurrency);
        Client GetClient(Guid guid);
        void UpdateClient(Client client);
        void DeleteClient(Guid guid);
    }
}