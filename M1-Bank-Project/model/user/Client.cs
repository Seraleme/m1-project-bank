﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace M1_Bank_Project.model.user
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class Client : IUser
    {
        public Guid guid { get; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string pin { get; set; }
        public Dictionary<string, decimal> currencies { get; set; }
        public string mainCurrencySymbol { get; set; }
        public bool block { get; set; }

        public int tries { get; set; }

        public Client(string firstname, string lastname, string mainCurrencySymbol)
        {
            guid = Guid.NewGuid();
            this.firstname = firstname;
            this.lastname = lastname;
            currencies = new Dictionary<string, decimal>();
            generateNewPin();
            this.mainCurrencySymbol = mainCurrencySymbol;
        }

        public Client(Guid guid, string firstname, string lastname, string pin, string mainCurrencySymbol, bool block,
            int tries)
        {
            this.guid = guid;
            this.firstname = firstname;
            this.lastname = lastname;
            this.pin = pin;
            this.mainCurrencySymbol = mainCurrencySymbol;
            this.block = block;
            this.tries = tries;
            currencies = new Dictionary<string, decimal>();
        }

        [JsonConstructor]
        public Client(Guid guid, string firstname, string lastname, string pin, Dictionary<string, decimal> currencies,
            string mainCurrencySymbol, bool block, int tries)
        {
            this.guid = guid;
            this.firstname = firstname;
            this.lastname = lastname;
            this.pin = pin;
            this.currencies = currencies;
            this.mainCurrencySymbol = mainCurrencySymbol;
            this.block = block;
            this.tries = tries;
        }

        public void generateNewPin()
        {
            var rand = new Random();
            string pin = "";
            for (int i = 0; i < 4; i++)
                pin += rand.Next(9);
            this.pin = pin;
        }
    }
}