﻿namespace M1_Bank_Project.model.user
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class Admin : IUser
    {
        public string firstname { get; }
        public string lastname { get; }
        public string pin { get; }

        public Admin()
        {
            this.firstname = "admin";
            this.lastname = "";
            this.pin = "1234";
        }
    }
}