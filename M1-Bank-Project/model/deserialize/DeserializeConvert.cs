﻿using System;

namespace M1_Bank_Project.model.deserialize
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class DeserializeConvert : IDeserialize
    {
        public double val { get; set; }

        public string getURL(params object[] args)
        {
            return String.Format(
                "https://free.currconv.com/api/v7/convert?apiKey=" + IDeserialize.KEY + "&q={0}&compact=y",
                args);
        }
    }
}