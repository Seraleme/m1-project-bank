﻿using System.Collections.Generic;

namespace M1_Bank_Project.model.deserialize
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public class DeserializeCurrencies : IDeserialize
    {
        public Dictionary<string, Currency> results { get; set; }

        public Currency get(string tag)
        {
            return results[tag];
        }

        public string getURL(params object[] args)
        {
            return $"https://free.currconv.com/api/v7/currencies?apiKey={IDeserialize.KEY}";
        }
    }

    public class Currency
    {
        public string currencyName { get; set; }
        public string currencySymbol { get; set; }
        public string id { get; set; }
    }
}