﻿using System.Windows.Controls;

namespace M1_Bank_Project.views
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class PageLoading : Page
    {
        public PageLoading()
        {
            InitializeComponent();
        }
    }
}