﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;
using M1_Bank_Project.views.admin;
using M1_Bank_Project.views.client;

namespace M1_Bank_Project.views
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class PageLogin : Page
    {
        private BankService _bankService;
        private MainWindow _mainWindow;

        public PageLogin(MainWindow mainWindow, BankService bankService)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
            _bankService = bankService;
        }

        private void onLogin(object sender, RoutedEventArgs e)
        {
            try
            {
                IUser connectUser = _bankService.connectUser(login.Text, pin.Password);
                if (connectUser == null)
                {
                    errorMessage.Foreground = (SolidColorBrush) new BrushConverter().ConvertFrom("#FFDC1212");
                    errorMessage.Text = "Incorrect login or password";
                }
                else if (connectUser is Admin)
                {
                    _mainWindow.PagesService.Content = new PageAdmin(connectUser, this, _bankService);
                    errorMessage.Text = "";
                }
                else
                {
                    _mainWindow.PagesService.Content = new PageClient((Client) connectUser, this, _bankService);
                    errorMessage.Text = "";
                }
            }
            catch (Exception exception)
            {
                if (exception.Message.Contains("tries"))
                    errorMessage.Foreground = Brushes.DarkOrange;
                else
                    errorMessage.Foreground = (SolidColorBrush) new BrushConverter().ConvertFrom("#FFDC1212");
                errorMessage.Text = exception.Message;
            }
        }

        public void diconnect()
        {
            login.Text = "";
            pin.Password = "";
            _mainWindow.PagesService.Content = this;
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                onLogin(sender, null);
        }
    }
}