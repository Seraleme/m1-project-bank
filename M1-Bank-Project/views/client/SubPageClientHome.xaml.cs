﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using M1_Bank_Project.model.deserialize;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.client
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class SubPageClientHome : Page
    {
        private Client _client;
        private FetchService _fetchService;
        private BankService _bankService;

        public SubPageClientHome(Client client, BankService bankService)
        {
            InitializeComponent();
            _client = client;
            _bankService = bankService;
            _fetchService = new FetchService();
            guid.Text = client.guid.ToString();

            int index = -1;
            bool notFound = true;
            foreach (var keyValuePair in bankService.currencies.results)
            {
                currentCurrency.Items.Add(new ComboBoxItem
                {
                    Content = keyValuePair.Value.id,
                    Uid = keyValuePair.Value.id
                });
                currentCurrencyAddRemove.Items.Add(new ComboBoxItem
                {
                    Content = keyValuePair.Value.id,
                    Uid = keyValuePair.Value.id
                });
                currentCurrencyExchangeTo.Items.Add(new ComboBoxItem
                {
                    Content = keyValuePair.Value.id,
                    Uid = keyValuePair.Value.id
                });


                if (notFound)
                {
                    index++;
                    if (keyValuePair.Key.Equals(client.mainCurrencySymbol))
                        notFound = false;
                }
            }

            if (!notFound)
                currentCurrency.SelectedIndex = index;

            setComboboxItemFrom();
            displayCurrencies();
        }

        public void setComboboxItemFrom()
        {
            currentCurrencyExchangeFrom.Items.Clear();
            foreach (var keyValuePair in _client.currencies)
            {
                currentCurrencyExchangeFrom.Items.Add(new ComboBoxItem
                {
                    Content = keyValuePair.Key,
                    Uid = keyValuePair.Key
                });
            }

            currentCurrencyExchangeFrom.SelectedIndex = 0;
        }

        public async void displayCurrencies()
        {
            decimal sum = 0;
            Dictionary<string, DeserializeConvert> dictionary = new Dictionary<string, DeserializeConvert>();
            StringBuilder stringBuilder = new StringBuilder();
            int amount = 0;
            foreach (var keyValuePair in _client.currencies)
            {
                stringBuilder.Append(keyValuePair.Key);
                stringBuilder.Append('_');
                stringBuilder.Append(_client.mainCurrencySymbol);
                stringBuilder.Append(',');
                if (++amount == 2)
                {
                    stringBuilder.Length--;
                    var result = await _fetchService.fetchDataDictionary<DeserializeConvert>(stringBuilder.ToString());
                    foreach (var kvp in result)
                        dictionary.Add(kvp.Key, kvp.Value);

                    amount = 0;
                    stringBuilder.Clear();
                }
            }

            if (amount != 0)
            {
                stringBuilder.Length--;
                var result = await _fetchService.fetchDataDictionary<DeserializeConvert>(stringBuilder.ToString());
                foreach (var kvp in result)
                    dictionary.Add(kvp.Key, kvp.Value);
            }

            panelUserMoney.Children.Clear();
            foreach (var keyValuePair in _client.currencies)
            {
                StackPanel line = new StackPanel();
                line.Orientation = Orientation.Horizontal;

                Currency currency = _bankService.currencies.get(keyValuePair.Key);

                line.Children.Add(createTextBlock(keyValuePair.Key));
                line.Children.Add(createTextBlock(
                    Math.Round(keyValuePair.Value, 3).ToString(CultureInfo.InvariantCulture) + " " +
                    currency.currencySymbol));

                sum += keyValuePair.Value *
                       Convert.ToDecimal(dictionary[$"{keyValuePair.Key}_{_client.mainCurrencySymbol}"].val);

                panelUserMoney.Children.Add(line);
            }

            Currency currency1 = _bankService.currencies.get(_client.mainCurrencySymbol);
            total.Text =
                $"{Math.Round(sum, 2)} {(currency1.currencySymbol == "" ? currency1.id : currency1.currencySymbol)}";
        }

        public TextBlock createTextBlock(string value)
        {
            TextBlock textBlock = new TextBlock
            {
                Text = value,
                Foreground = Brushes.White,
                Margin = new Thickness(5, 0, 0, 5),
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Width = 150,
                FontSize = 18
            };
            return textBlock;
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            string value = amountMoney.Text.Replace(",", ".").Replace(" ", "");
            decimal amount;
            if (!Decimal.TryParse(value, out amount))
            {
                MessageBox.Show("[Error]\nInvalid number");
                return;
            }

            _bankService.addMoney(_client, Math.Abs(amount),
                ((ComboBoxItem) currentCurrencyAddRemove.SelectedItem).Uid);

            amountMoney.Text = "0";
            displayCurrencies();
            setComboboxItemFrom();
        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            string value = amountMoney.Text.Replace(",", ".").Replace(" ", "");
            decimal amount;
            if (!Decimal.TryParse(value, out amount))
            {
                MessageBox.Show("[Error]\nInvalid number");
                return;
            }

            _bankService.addMoney(_client, -Math.Abs(amount),
                ((ComboBoxItem) currentCurrencyAddRemove.SelectedItem).Uid);

            amountMoney.Text = "0";
            displayCurrencies();
            setComboboxItemFrom();
        }

        private async void ConvertCurrencies_Click(object sender, RoutedEventArgs e)
        {
            if (currentCurrencyExchangeFrom.Text.Equals(currentCurrencyExchangeTo.Text))
                return;

            string value = MoneyExchangeFrom.Text.Replace(",", ".").Replace(" ", "");
            decimal amountFrom;
            if (!Decimal.TryParse(value, out amountFrom))
            {
                MessageBox.Show("[Error]\nInvalid number");
                return;
            }

            string keyConvert = currentCurrencyExchangeFrom.Text + "_" + currentCurrencyExchangeTo.Text;
            var dictionary = await _fetchService.fetchDataDictionary<DeserializeConvert>(keyConvert);

            _bankService.addMoney(_client, -Math.Abs(amountFrom), currentCurrencyExchangeFrom.Text);
            _bankService.addMoney(_client, Math.Abs(amountFrom) * Convert.ToDecimal(dictionary[keyConvert].val),
                currentCurrencyExchangeTo.Text);

            MoneyExchangeFrom.Text = "0";
            displayCurrencies();
            setComboboxItemFrom();
        }

        private void currentCurrency_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _client.mainCurrencySymbol = ((ComboBoxItem) currentCurrency.SelectedItem).Uid;
            _bankService._clientDataAccess.UpdateClient(_client);
            displayCurrencies();
        }
    }
}