﻿using System.Windows;
using System.Windows.Controls;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.client
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class PageClient : Page
    {
        private Client _client;
        private BankService _bankService;
        private PageLogin _pageLogin;

        private SubPageClientHome _subPageClientHome;

        public PageClient(Client client, PageLogin pageLogin, BankService bankService)
        {
            InitializeComponent();
            _client = client;
            _pageLogin = pageLogin;
            _bankService = bankService;
            _subPageClientHome = new SubPageClientHome(client, bankService);
            PagesService.Content = _subPageClientHome;
            userLastname.Text = client.lastname;
            userFirstname.Text = client.firstname;
        }

        private void OnDisconnect(object sender, RoutedEventArgs e)
        {
            _pageLogin.diconnect();
        }

        private void retrieveMoney_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}