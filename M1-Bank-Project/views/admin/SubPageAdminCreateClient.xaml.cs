﻿using System;
using System.Windows;
using System.Windows.Controls;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.admin
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class SubPageAdminCreateClient : Page
    {
        private BankService _bankService;

        public SubPageAdminCreateClient(BankService bankService)
        {
            InitializeComponent();
            _bankService = bankService;

            foreach (var keyValuePair in bankService.currencies.results)
                comboBox.Items.Add(new ComboBoxItem
                {
                    Content = keyValuePair.Value.id + " - " + keyValuePair.Value.currencyName,
                    Uid = keyValuePair.Value.id
                });
        }

        private void CreateUser(object sender, RoutedEventArgs e)
        {
            if (lastname.Text.Length != 0 && firstname.Text.Length != 0 &&
                ((ComboBoxItem) comboBox.SelectedItem).Uid.Length != 0)
            {
                try
                {
                    var client = _bankService.createClient(firstname.Text, lastname.Text,
                        ((ComboBoxItem) comboBox.SelectedItem).Uid);
                    MessageBox.Show("The client has been created !\n\nPin: " + client.pin + "\nGuid: " + client.guid +
                                    "\n\nThe guid has been added to your clipboard !");
                    try
                    {
                        System.Windows.Clipboard.SetDataObject(client.guid.ToString());
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("An error occurred while copying to the clipboard.");
                    }

                    lastname.Text = "";
                    firstname.Text = "";
                }
                catch (Exception)
                {
                    MessageBox.Show("[Error]\nAn error occurred while creating the client.");
                }
            }
        }
    }
}