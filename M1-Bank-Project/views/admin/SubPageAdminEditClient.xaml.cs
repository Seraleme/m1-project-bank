﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using M1_Bank_Project.model.deserialize;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.admin
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class SubPageAdminEditClient : Page
    {
        private PageAdmin _pageAdmin;
        private Client _client;
        private BankService _bankService;
        private SubPageAdminListClient _subPageAdminListClient;
        private FetchService _fetchService;

        public SubPageAdminEditClient(PageAdmin pageAdmin, SubPageAdminListClient subPageAdminListClient, Client client,
            BankService bankService)
        {
            InitializeComponent();
            _client = client;
            _bankService = bankService;
            _subPageAdminListClient = subPageAdminListClient;
            _pageAdmin = pageAdmin;
            _fetchService = new FetchService();
            name.Text = client.lastname + " " + client.firstname;
            guid.Text = client.guid.ToString();

            displayTries();
            displayButtonBlock();
            displayCurrencies();
        }

        public async void displayCurrencies()
        {
            panelUserMoney.Children.Clear();
            decimal sum = 0;
            Dictionary<string, DeserializeConvert> dictionary = new Dictionary<string, DeserializeConvert>();
            StringBuilder stringBuilder = new StringBuilder();
            int amount = 0;
            foreach (var keyValuePair in _client.currencies)
            {
                stringBuilder.Append(keyValuePair.Key);
                stringBuilder.Append('_');
                stringBuilder.Append(_client.mainCurrencySymbol);
                stringBuilder.Append(',');
                if (++amount == 2)
                {
                    stringBuilder.Length--;
                    var result = await _fetchService.fetchDataDictionary<DeserializeConvert>(stringBuilder.ToString());
                    foreach (var kvp in result)
                        dictionary.Add(kvp.Key, kvp.Value);

                    amount = 0;
                    stringBuilder.Clear();
                }
            }

            if (amount != 0)
            {
                stringBuilder.Length--;
                var result = await _fetchService.fetchDataDictionary<DeserializeConvert>(stringBuilder.ToString());
                foreach (var kvp in result)
                    dictionary.Add(kvp.Key, kvp.Value);
            }

            foreach (var keyValuePair in _client.currencies)
            {
                StackPanel line = new StackPanel();
                line.Orientation = Orientation.Horizontal;

                Currency currency = _bankService.currencies.get(keyValuePair.Key);

                line.Children.Add(createTextBlock(keyValuePair.Key));
                line.Children.Add(createTextBlock(
                    Math.Round(keyValuePair.Value, 3).ToString(CultureInfo.InvariantCulture) + " " +
                    currency.currencySymbol));

                sum += keyValuePair.Value *
                       Convert.ToDecimal(dictionary[$"{keyValuePair.Key}_{_client.mainCurrencySymbol}"].val);

                panelUserMoney.Children.Add(line);
            }

            Currency currency1 = _bankService.currencies.get(_client.mainCurrencySymbol);
            total.Text =
                $"{Math.Round(sum, 2)} {(currency1.currencySymbol == "" ? currency1.id : currency1.currencySymbol)}";
        }

        public void displayButtonBlock()
        {
            if (_client.block)
            {
                block.Background = Brushes.Green;
                block.Content = "Unblock";
                blockMessage.Text = "block";
                blockMessage.Foreground = Brushes.Crimson;
            }
            else
            {
                block.Background = (SolidColorBrush) new BrushConverter().ConvertFrom("#FF5F2424");
                block.Content = "Block";
                blockMessage.Text = "unblock";
                blockMessage.Foreground = Brushes.Green;
            }
        }

        public void displayTries()
        {
            if (_client.tries == 0)
                triesMessage.Foreground = Brushes.White;
            else if (_client.tries is 1 or 2)
                triesMessage.Foreground = Brushes.DarkOrange;
            else
                triesMessage.Foreground = (SolidColorBrush) new BrushConverter().ConvertFrom("#FFDC1212");
            triesMessage.Text = $"The client has {_client.tries} tries.";
        }

        public TextBlock createTextBlock(string value)
        {
            TextBlock textBlock = new TextBlock
            {
                Text = value,
                Foreground = Brushes.White,
                Margin = new Thickness(5, 0, 0, 5),
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Width = 150,
                FontSize = 18
            };
            return textBlock;
        }

        private void block_Click(object sender, RoutedEventArgs e)
        {
            _bankService.toggleBlock(_client);
            displayButtonBlock();
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Delete the client " + _client.firstname + " " + _client.lastname + " ?",
                "Delete Client",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                _bankService.deleteClient(_client.guid);
                _subPageAdminListClient.refreshValue();
                _pageAdmin.PagesService.Content = _subPageAdminListClient;
            }
        }

        private void changePin_Click(object sender, RoutedEventArgs e)
        {
            _client.generateNewPin();
            _bankService._clientDataAccess.UpdateClient(_client);
            MessageBox.Show("The client's pin has been changed !\nPin: " + _client.pin);
        }

        private void resetTries_Click(object sender, RoutedEventArgs e)
        {
            _client.tries = 0;
            _bankService._clientDataAccess.UpdateClient(_client);
            displayTries();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            _pageAdmin.PagesService.Content = _subPageAdminListClient;
        }

        private void copy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Clipboard.SetDataObject(guid.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred while copying to the clipboard.");
                throw;
            }
        }
    }
}