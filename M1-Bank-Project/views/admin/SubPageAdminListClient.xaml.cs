﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.admin
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class SubPageAdminListClient : Page
    {
        private BankService _bankService;
        private FetchService _fetchService;
        private PageAdmin _pageAdmin;

        public SubPageAdminListClient(BankService bankService, PageAdmin pageAdmin)
        {
            InitializeComponent();
            _bankService = bankService;
            _pageAdmin = pageAdmin;
            _fetchService = new FetchService();
            refreshValue();
        }

        public void refreshValue()
        {
            panel.Children.Clear();
            List<Client> allClient = _bankService.getAllClient();
            for (var i = 0; i < allClient.Count && i < 100; i++)
            {
                Client client = allClient[i];
                if (!client.lastname.ToLower().Contains(search.Text.ToLower()) &&
                    !client.firstname.ToLower().Contains(search.Text.ToLower()))
                    continue;
                StackPanel line = new StackPanel();
                line.Orientation = Orientation.Horizontal;
                line.VerticalAlignment = VerticalAlignment.Center;
                line.Style = (Style) FindResource("StackPanelStyle");
                line.MouseDown += delegate(object sender, MouseButtonEventArgs e)
                {
                    OnSelectClient(sender, e, client);
                };

                line.Children.Add(createTextBlock(client.lastname));
                line.Children.Add(createTextBlock(client.firstname));
                line.Children.Add(createTextBlock(client.mainCurrencySymbol));

                var textBlock = createTextBlock(client.block ? "Block" : "Unblock");
                textBlock.Foreground = (client.block) ? Brushes.Crimson : Brushes.White;
                line.Children.Add(textBlock);

                var textBlockTries = createTextBlock($"{client.tries} tries");
                if (client.tries == 0)
                    textBlockTries.Foreground = Brushes.White;
                else if (client.tries is 1 or 2)
                    textBlockTries.Foreground = Brushes.DarkOrange;
                else
                    textBlockTries.Foreground = (SolidColorBrush) new BrushConverter().ConvertFrom("#FFDC1212");
                line.Children.Add(textBlockTries);

                panel.Children.Add(line);
            }
        }

        public void OnSelectClient(object sender, MouseButtonEventArgs e, Client client)
        {
            _pageAdmin.PagesService.Content = new SubPageAdminEditClient(_pageAdmin, this, client, _bankService);
        }

        public void reinitializeValue()
        {
            search.Text = "";
            refreshValue();
        }

        public TextBlock createTextBlock(string format, string value)
        {
            TextBlock textBlock = new TextBlock
            {
                Text = String.Format(format, value),
                Foreground = Brushes.White,
                Margin = new Thickness(0, 5, 0, 5),
                VerticalAlignment = VerticalAlignment.Center,
                TextAlignment = TextAlignment.Center,
                Width = 195,
                FontSize = 18
            };
            return textBlock;
        }

        public TextBlock createTextBlock(string value)
        {
            return createTextBlock("{0}", value);
        }

        private void OnSearch(object sender, RoutedEventArgs e)
        {
            refreshValue();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                refreshValue();
        }
    }
}