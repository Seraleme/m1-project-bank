﻿using System.Windows;
using System.Windows.Controls;
using M1_Bank_Project.model.user;
using M1_Bank_Project.services;

namespace M1_Bank_Project.views.admin
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class PageAdmin : Page
    {
        private SubPageAdminCreateClient _subPageAdminCreateClient;
        private SubPageAdminListClient _subPageAdminListClient;
        private PageLogin _pageLogin;

        public PageAdmin(IUser user, PageLogin pageLogin, BankService bankService)
        {
            InitializeComponent();
            _pageLogin = pageLogin;
            userLastname.Text = user.lastname;
            userFirstname.Text = user.firstname;
            _subPageAdminCreateClient = new SubPageAdminCreateClient(bankService);
            _subPageAdminListClient = new SubPageAdminListClient(bankService, this);
            PagesService.Content = _subPageAdminListClient;
        }

        private void ChangePageCreateUser(object sender, RoutedEventArgs e)
        {
            PagesService.Content = _subPageAdminCreateClient;
        }

        private void ChangePageListUser(object sender, RoutedEventArgs e)
        {
            _subPageAdminListClient.reinitializeValue();
            PagesService.Content = _subPageAdminListClient;
        }

        private void OnDisconnect(object sender, RoutedEventArgs e)
        {
            _pageLogin.diconnect();
        }
    }
}