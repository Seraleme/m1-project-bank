-- Clients definition

CREATE TABLE Clients
(
    client_id TEXT(36) NOT NULL,
    firstname TEXT     NOT NULL,
    lastname  TEXT     NOT NULL,
    currency  TEXT DEFAULT 'USD' NOT NULL,
    pin       TEXT(4)  NOT NULL,
    CONSTRAINT Clients_PK PRIMARY KEY (client_id)
);


-- Clients_Currencies definition

CREATE TABLE "Clients_Currencies"
(
    client_id   TEXT(36) not null
        references Clients,
    currency_id TEXT     not null,
    amount      REAL default 0 not null,
    constraint Clients_Currencies_PK
        primary key (client_id, currency_id)
);