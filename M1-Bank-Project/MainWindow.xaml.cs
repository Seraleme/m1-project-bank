﻿using System;
using System.Windows;
using M1_Bank_Project.dataAccess;
using M1_Bank_Project.model.deserialize;
using M1_Bank_Project.services;
using M1_Bank_Project.views;

namespace M1_Bank_Project
{
    /**
     * @author PONCIN Séverin
     * @author MEEDENDORP Thomas
     * @author SEDLAK Clément
     * @author LEROY Jean-Baptiste
     */
    public partial class MainWindow : Window
    {
        private readonly FetchService _fetchService;

        private DeserializeCurrencies currencies;
        private BankService _bankService;

        public MainWindow()
        {
            InitializeComponent();
            PagesService.Content = new PageLoading();
            _fetchService = new FetchService();
            try
            {
                InitializeCurrency();
            }
            catch (Exception)
            {
                MessageBox.Show("[Error]\nAn error occurred while retrieving data from the api.");
            }
        }

        private async void InitializeCurrency()
        {
            this.currencies = await _fetchService.fetchData<DeserializeCurrencies>();

            IClientDataAccess clientDataAccess;
            try
            {
                clientDataAccess = new ClientDBAccess();
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to connect to the database, automatic switch to json storage.");
                clientDataAccess = new ClientJsonAccess();
            }

            this._bankService = new BankService(clientDataAccess, currencies);
            PagesService.Content = new PageLogin(this, _bankService);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (_bankService._clientDataAccess is ClientDBAccess)
                ((ClientDBAccess) _bankService._clientDataAccess).CloseConnection();
        }
    }
}