# M1 Project Bank

## Usage

### Admin account :
Login `admin`, Password `1234`

### Client account :

As login use the guid obtained from the admin panel (click on a client from the list of clients on the admin panel to access its data).

As a password use the pin obtained when creating the client, or regenerate one from the admin panel.

## Contributor

- PONCIN Séverin
- MEEDENDORP Thomas
- SEDLAK Clément
- LEROY Jean-Baptiste
